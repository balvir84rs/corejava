package testDB;

import java.sql.*;

/**
 * Created by balvir on 02/17/2017.
 */
public class   testDBClass {
    public static void main (String arg[]){

        System.out.println("test if ms-access db can be connected");
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn =DriverManager.getConnection("jdbc:mysql://localhost/world?" +
                            "user=root&password=root");

            // Do something with the Connection
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery("Select * from country");
            while(rs.next()){

                System.out.println(rs.getString("Name")+"---"+rs.getString("Code"));
            }

        } catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
}
