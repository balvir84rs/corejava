/**
 * 
 */
package testDB;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author balvir
 *
 */
public class DBConnection {
	public static Connection getConnection() {
        try  {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con =DriverManager.getConnection("jdbc:mysql://localhost/world?" +
                    "user=root&password=root");
            return con;
        }
        catch(Exception ex) {
            System.out.println("Database.getConnection() Error -->" + ex.getMessage());
            return null;
        }
    }

     public static void close(Connection con) {
        try  {
            con.close();
        }
        catch(Exception ex) {
        }
    }

}
