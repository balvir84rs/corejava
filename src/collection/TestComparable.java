package collection;

/**
 * Created by balvir on 03/09/2017.
 */
import java.util.*;
import java.io.*;
public class TestComparable{
    public static void main(String args[]){
        ArrayList<Student> al=new ArrayList<Student>();
        Student st1 = new Student(101,"Vijay",23);
        Student st2 = new Student(106,"Ajay",27);
        Student st3 = new Student(105,"Jai",21);
        //Add the elements into the list
        al.add(st1);
        al.add(st2);
        al.add(st3);

        Collections.sort(al);
        for(Student st:al){
            System.out.println(st.rollno+" "+st.name+" "+st.age);
        }
    }
}