package collection;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by balvir on 02/14/2017.
 */
 /*
Java ArrayList class can contain duplicate elements.
Java ArrayList class maintains insertion order.
Java ArrayList class is non synchronized.
Java ArrayList allows random access because array works at the index basis.
In Java ArrayList class, manipulation is slow because a lot of shifting needs to be occurred if any element is removed from the array list.
 * *********/

public class TestList {
    List<String> list = new ArrayList<String>();
    public static void main(String arg[]){

        System.out.println("Welcome to the the testing List interface");
        TestList list=new TestList();
        list.listCanContainDuplicates();
        list.listmaintainsInsertionOrder();
        list.withoutGenerics();
        list.testAddAll();
        list.testRetainAll();
        list.testRemove();
        list.testListIteration();
    }

    private void testListIteration() {
        System.out.println("you are here to check different ways of ");
    }

    private void testRetainAll() {
        System.out.println("testing retainAll() method");
        List<String> list1 = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();

        list1.add("A");
        list1.add("B");
        list2.add("PP");
        list2.add("QQ");
        list2.add("A");
        list2.add("B");

        list2.retainAll(list1);

        System.out.println(list2);
    }


    private void testAddAll() {

        List<String> list1 = new ArrayList<String>();
        List<String> list2 = new ArrayList<String>();

        list1.add("A");
        list1.add("B");
        list2.add("PP");
        list2.add("QQ");

        list1.addAll(list2);

        System.out.println("After adding elements to another list");
        for(String s: list1){
            System.out.println(s);
        }
    }

    private void withoutGenerics() {
        System.out.println("Call without generics");
        List list = new ArrayList();
        list.add("ABC");        list.add(1);
        list.add("0.44");

        Iterator itr = list.iterator();
        while(itr.hasNext()){
          System.out.println(itr.next());
      }
    }

    private void listCanContainDuplicates(){
        System.out.println("To check if list has duplicates");
        list.add("Ram");
        list.add("Shyam");
        list.add("Ram");
        list.add("Krisha");

        //Traverse the list
        for(String s: list) {
            System.out.println(s);

            if(s.equalsIgnoreCase("RAM")){
                System.out.println("yes, it is containing duplicates");
            }
        }
        //list=null;
        //check if the this contains duplicates
    }

    private void listmaintainsInsertionOrder() {
        System.out.println("to check if list maintains insertion order");
        list.add("ABC");
        list.add("EFG");
        list.add("Navodaya");

        for(String s:list){
            System.out.println(s);
        }
       // list=null;
    }
    private void testRemove(){
        System.out.println("inside to check remove ");
        List<String> str = new ArrayList<>();
        list.add("ANC");
        list.add("BBB");
        list.add("CCCC");

        System.out.println("list elements:"+list);
        list.remove(1);
        System.out.println("list after delete:"+list);
    }
}
