package corejava;

/**
 * Created by balvir on 03/07/2017.
 */

        import java.util.ArrayList;
        import java.util.List;

public class TestStaticBlock {

        private static List<String> list;

        static{
//created required instances
//create ur in-memory objects here
list = new ArrayList<String>();
list.add("one");
list.add("two");
list.add("three");
list.add("four");
}

public void testList(){
System.out.println(list);
}

public static void main(String a[]){
    System.out.println("main method");
    TestStaticBlock msb = new TestStaticBlock();
msb.testList();
}
}