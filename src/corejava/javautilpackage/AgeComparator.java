package corejava.javautilpackage;

import java.util.Comparator;

/**
 * Created by balvir on 02/14/2017.
 */
class AgeComparator implements Comparator {

public int compare(Object o1,Object o2){
        Family f1=(Family)o1;
        Family f2=(Family)o2;

        if(f1.age==f2.age)
        return 0;
        else if(f1.age>f2.age)
        return 1;
        else
        return -1;
        }
        }
