package corejava.javautilpackage;

/**
 * Created by balvir on 02/14/2017.
 */

import java.util.HashSet;
import java.util.Set;
import java.util.StringJoiner;
import java.util.TreeSet;

/*****
 * Java HashSet class

 Java HashSet class is used to create a collection that uses a hash table for storage. It inherits the AbstractSet class and implements Set interface.
 The important points about Java HashSet class are:
 HashSet stores the elements by using a mechanism called hashing.
 HashSet contains unique elements only.
 Difference between List and Set
 List can contain duplicate elements whereas Set contains unique elements only.
 Hierarchy of HashSet class
 The HashSet class extends AbstractSet class which implements Set interface. The Set interface inherits Collection and Iterable interfaces in hierarchical order.
 * *******/
public class TestSetCollection {

    public static void main(String arg[]){
        //sout+enter
        System.out.println("Welcome to test HashSet/Set in collection");
        TestSetCollection tsc = new TestSetCollection();
        tsc.testSetContainsUniqueOnly();
        tsc.testTreeSetMaintainsAscendingOrder();
    }

    private void testTreeSetMaintainsAscendingOrder() {
        System.out.println("Test if TreeSet maintains ascending order");

        Set set = new TreeSet();
        set.add("A");
        set.add("C");
        set.add("K");
        set.add("B");

        System.out.println(set);
    }

    private void testSetContainsUniqueOnly() {
        Set<String> set = new HashSet<String>();

        set.add("Ram");
        set.add("Rahim");
        set.add("Kabir");
        set.add("Tulsi");
        set.add("Ram");//Adding duplicate

        //Traverse the set
        for(String s:set){
            System.out.println(s);
        }
    }
}
