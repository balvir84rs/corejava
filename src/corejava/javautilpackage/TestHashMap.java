package corejava.javautilpackage;

/**
 * Created by balvir on 02/15/2017.
 */

import java.util.HashMap;
import java.util.Map;

/**
 *
 o  A HashMap contains values based on the key.
 o	It contains only unique elements.
 o	It may have one null key and multiple null values.
 o	It maintains no order.

 * */
/*class Book1 {
    int id;
    String name, author, publisher;
    int quantity;
}*/
public class TestHashMap {
    public static void main(String arg[]){
        //creating map of books
        Map<Integer,Book> bookMap = new HashMap<Integer,Book>();
        //creating books
        Book book1 = new Book(101,"Let us C","Yashwant Kanetkar","BPB",8);
        Book book2 = new Book(102,"Data Communications & Networking","Forouzan","Mc Graw Hill",4);
        Book book3 = new Book(103,"Operating System","Galvin","Wiley",6);
        //Adding books to map
        bookMap.put(1,book1);
        bookMap.put(2,book2);
        bookMap.put(3,book3);

        //Traversing the books
        for(Map.Entry<Integer,Book> entry: bookMap.entrySet()){
            int key = entry.getKey();
            Book b = entry.getValue();

            System.out.println(key+" Details:");
            System.out.println(b.id+" "+b.name+" "+b.author+" "+b.publisher+" "+b.quantity);
        }
        TestHashMap thm = new TestHashMap();
        thm.mapCantContainsDuplicateKeys();
    }

    private void mapCantContainsDuplicateKeys() {
        System.out.println("Oh realy ,let me check here");

        Map<Integer, String> map = new HashMap<Integer,String>();
        map.put(1,"A");
        map.put(1,"A");
        map.put(2,"B");
        map.put(2,"B");

        for(Map.Entry entry:map.entrySet()){
            System.out.println(entry.getKey()+""+entry.getValue());
        }
    }
}

