package corejava.javautilpackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by balvir on 02/14/2017.
 */
public class CollectionsExample {

    List<String> list = new ArrayList<String>();

    public static void main(String args[]){
        CollectionsExample ce = new CollectionsExample();
        System.out.println("Welcome to learn collections here!!");
        //List of family members
        ce.familymembers();
        ce.familyInSortbyName();
    }

    public void familyInSortbyName() {
        System.out.println("Members in family in alphabetical sort order-------");

        Collections.sort(list);
        for(String s:list){
            System.out.println(s);
        }
    }

    public void familymembers() {
        System.out.println("List of family order");

        list.add("Kaptan Singh");
        list.add("Harnath Singh");
        list.add("Amit Satsangi");
        list.add("Agam Satsangi");
        list.add("Apaar Satsangi");
        list.add("Mehar Satsangi");

        for(String s:list){
            System.out.println(s);
        }


    }
}
