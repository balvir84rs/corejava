package corejava.javautilpackage;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by balvir on 02/15/2017.
 */
public class TestMapInterface {
    public static void main(String arg[]){
        System.out.println("Welcome to test Map interface under java.util package");
        TestMapInterface tmi = new TestMapInterface();

        tmi.testMapFunctionality();
    }

    private void testMapFunctionality() {
        System.out.println("Hello are you sure to chek Map");
        Map<Integer, String> map = new HashMap<Integer, String>();

        map.put(10, "List");
        map.put(20,"Set");
        map.put(30,"Map");

        //Traversing of Map
        //1 - Using Map.Entry
        System.out.println("Traversing via Map.Entry");
        for(Map.Entry m:map.entrySet()){
            System.out.println(m.getKey()+""+m.getValue());
        }
        //2- need to convert map to set
        Set set = map.entrySet();
        Iterator itr = set.iterator();
        System.out.println("Traversing via map converting to set");
        while(itr.hasNext()){
            //Converting to Map.Entry so that we can get key and value separately
            Map.Entry entry = (Map.Entry)itr.next();
            System.out.println(entry.getKey()+""+entry.getValue());

        }

    }
}
