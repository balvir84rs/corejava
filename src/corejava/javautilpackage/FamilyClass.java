package corejava.javautilpackage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by balvir on 02/14/2017.
 */
public class FamilyClass {
    public static void main(String arg[]){

        List list = new ArrayList();
        list.add(new Family("Kaptan Singh", "farmer",45));
        list.add(new Family("Harnath Singh", "Army", 38));
        list.add(new Family("Balvir Singh", "IT", 33));

        System.out.println("sorting by age...");

        Collections.sort(list,new AgeComparator());
        Iterator itr2=list.iterator();
        while(itr2.hasNext()){

            Family st=(Family)itr2.next();
            System.out.println(st.name+" "+st.age);
        }

    }
}
