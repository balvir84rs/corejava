package corejava.javautilpackage;

import java.util.LinkedList;
import java.util.List;

import static java.lang.System.out;

/**
 * Created by balvir on 02/14/2017.
 */
/*
*
Java LinkedList class can contain duplicate elements.
Java LinkedList class maintains insertion order.
Java LinkedList class is non synchronized.
In Java LinkedList class, manipulation is fast because no shifting needs to be occurred.
Java LinkedList class can be used as list, stack or queue.
* */

class Book{

    int id;
    String name,author,publisher;
    int quantity;

    //create constructor
    public Book(int id, String name, String author, String publisher, int quantity) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.quantity = quantity;
    }
}

public class LinkedListExample {

    public static void main(String args[]){
        System.out.println("Welcome to the to LinkedList");

        List<Book> bookList = new LinkedList<Book>();

        Book book1 = new Book(101,"Let us C","Yashwant Kanetkar","BPB",8);
        Book book2 = new Book(102,"Data Communications & Networking","Forouzan","Mc Graw Hill",4);
        Book book3 = new Book(103,"Operating System","Galvin","Wiley",6);

        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);

        for(Book s:bookList){
            System.out.println(s.id+" "+s.name);
        }

    }
}
