package corejava.javalangpackage;

import java.lang.Boolean;

/**
 * Created by balvir on 02/14/2017.
 */
public class TestJavaLangPackage {

    public static void main(String arg[]) {
        System.out.println("Welcome to practise java.lang package");
        //testing boolean
        //String s= testBoolean();
        booleanToString();
    }

    private static String testBoolean() {
        Boolean blnObj2 = new Boolean("false");
        Boolean blnObj1 = new Boolean(true);
        //print value of Boolean objects
        System.out.println(blnObj1);
        System.out.println(blnObj2);
        return "";
    }

    public static void booleanToString(){
        //construct Boolean object
        Boolean blnObj = new Boolean("true");

        //use toString method of Boolean class to convert it into String
        String str = blnObj.toString();
        System.out.println(str);
    }
}
